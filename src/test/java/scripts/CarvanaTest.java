package scripts;

import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.Waiter;

public class CarvanaTest extends Base{

    @Test(priority = 1, description = "Validate Carvana home page title and url")
    public void validateCarvanaHomePageTitleAndUrl(){
        driver.get("https://www.carvana.com");
        Assert.assertEquals(driver.getTitle(), "Carvana | Buy & Finance Used Cars Online | At Home Delivery");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/");
    }

    @Test(priority = 2, description = "Validate the Carvana logo")
    public void validateTheCarvanaLogo(){
        driver.get("https://www.carvana.com");
        Assert.assertTrue(carvanaHomePage.logo.isDisplayed());
    }

    @Test(priority = 3, description = "Validate the main navigation section items")
    public void validateNavigationSectionItems(){
        driver.get("https://www.carvana.com");
        String[] navigationSectionItemsList = {"HOW IT WORKS", "ABOUT CARVANA", "SUPPORT & CONTACT"};
        for (int i = 0; i < navigationSectionItemsList.length; i++) {
            Waiter.pause(5);
            Assert.assertTrue(carvanaHomePage.navigationSectionItems.get(i).isDisplayed());
            Assert.assertEquals(carvanaHomePage.navigationSectionItems.get(i).getText(), navigationSectionItemsList[i]);
        }

    }

    @Test(priority = 4, description = "Validate the sign in error message")
     public void validateSignInErrorMessage(){
        driver.get("https://www.carvana.com/");
        carvanaHomePage.signInButton.click();
        Assert.assertTrue(carvanaHomePage.signInModal.isDisplayed());
        carvanaHomePage.emailInputBox.sendKeys("johndoe@gmail.com" + Keys.ENTER);
        carvanaHomePage.passwordInputBox.sendKeys("abcd1234" + Keys.ENTER);
        carvanaHomePage.signInSubmitButton.click();
        Assert.assertTrue(carvanaHomePage.errorMessage.isDisplayed());
        Assert.assertEquals(carvanaHomePage.errorMessage.getText(), "Email address and/or password combination is incorrect\n" +
                "Please try again or reset your password." );

    }


    @Test(priority = 5, description = "Validate the search filter options and search button")
    public void validateSearchFilterOption(){
        driver.get("https://www.carvana.com/");
        Waiter.pause(5);
        carvanaHomePage.searchCarsButton.click();
        Waiter.pause(10);
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/cars");
        String[] filterOptions = {"PAYMENT & PRICE", "MAKE & MODEL", "BODY TYPE", "YEAR & MILEAGE", "FEATURES", "MORE FILTERS"};
        for (int i = 0; i < filterOptions.length; i++) {
            Assert.assertTrue(searchOptionsFilterPage.searchOptionsFilterMenu.get(i).isDisplayed());
            Assert.assertEquals(searchOptionsFilterPage.searchOptionsFilterMenu.get(i).getText(), filterOptions[i]);
        }

    }

     /*
        Test name = Validate the search result tiles
Test priority = 6
Given user is on "https://www.carvana.com/"
When user clicks on "SEARCH CARS" link
Then user should be routed to "https://www.carvana.com/cars"
When user enters "mercedes-benz" to the search input box
And user clicks on "GO" button in the search input box
Then user should see "mercedes-benz" in the url
And validate each result tile

         */
    @Test(priority = 6, description = "Validate the search result tiles")
    public void validateTheSearchResultTiles(){
        driver.get("https://www.carvana.com/");
        Waiter.pause(5);
        carvanaHomePage.searchCarsButton.click();
        Waiter.pause(5);
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/cars");
        searchOptionsFilterPage.searchBarInputBox.sendKeys("mercedes-benz");
        searchOptionsFilterPage.goButton.click();

        Waiter.pause(5);
        Assert.assertTrue(driver.getCurrentUrl().contains("mercedes-benz"));

        for (int i = 0; i < mercedesBenzPage.tileImages.size(); i++) {
            Assert.assertTrue(mercedesBenzPage.tileImages.get(i).isDisplayed());
            Assert.assertTrue(mercedesBenzPage.addFavoriteButtons.get(i).isDisplayed());
            Assert.assertTrue(mercedesBenzPage.inventoryTypes.get(i).isDisplayed());
            Assert.assertTrue(mercedesBenzPage.inventoryTypes.get(i).getText() != null);
            Assert.assertTrue(mercedesBenzPage.yearMakeModelInformation.get(i).isDisplayed());
            Assert.assertTrue(mercedesBenzPage.yearMakeModelInformation.get(i).getText() != null);
            Assert.assertTrue(mercedesBenzPage.trimMileageInformation.get(i).isDisplayed());
            Assert.assertTrue(mercedesBenzPage.trimMileageInformation.get(i).getText() != null);
            Assert.assertTrue(mercedesBenzPage.prices.get(i).isDisplayed());
            Assert.assertTrue(Integer.parseInt(mercedesBenzPage.prices.get(i).getText().replaceAll("[^0-9]","")) > 0);
            Assert.assertTrue(mercedesBenzPage.monthlyPayments.get(i).isDisplayed());
            Assert.assertTrue(mercedesBenzPage.monthlyPayments.get(i).getText() != null);
            Assert.assertTrue(mercedesBenzPage.downPayments.get(i).isDisplayed());
            Assert.assertTrue(mercedesBenzPage.downPayments.get(i).getText() != null);

        }

    }
}
