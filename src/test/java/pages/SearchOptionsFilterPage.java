package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchOptionsFilterPage {
    public SearchOptionsFilterPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }
    @FindBy(css = "div[data-qa='menu-flex']>button")
    public List<WebElement> searchOptionsFilterMenu;

    @FindBy(css = "input[data-qa='search-bar-input']")
    public WebElement searchBarInputBox;

    @FindBy(css = "button[data-qa='go-button']")
    public WebElement goButton;


}
