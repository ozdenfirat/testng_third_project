package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class MercedesBenzPage {
    public MercedesBenzPage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "div[class='result-tile']>a picture")
    public List<WebElement> tileImages;

    @FindBy(css = "div[class='result-tile']>a svg")
    public List<WebElement> addFavoriteButtons;

    @FindBy(css = "div[class^='inventory-type-variant']")
    public List<WebElement> inventoryTypes;

    @FindBy(css = "div[data-qa='make-model']")
    public List<WebElement> yearMakeModelInformation;

    @FindBy(css = "div[data-qa='trim-mileage']")
    public List<WebElement> trimMileageInformation;

    @FindBy(css = ".price")
    public List<WebElement> prices;

    @FindBy(css = ".monthly-payment")
    public List<WebElement> monthlyPayments;

    @FindBy(css = ".down-payment")
    public List<WebElement> downPayments;



}
