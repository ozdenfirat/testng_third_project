package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CarvanaHomePage {
    public CarvanaHomePage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "div[class='Logo__LogoWrapper-sc-14r2405-0 fSZhVx']")
    public WebElement logo;


    @FindBy(css = "div[data-qa='navigation-wrapper']>div>a" )
    public List<WebElement> navigationSectionItems;

    @FindBy(css = "div[data-qa='sign-in-wrapper']")
    public WebElement signInButton;

    @FindBy(css = "div[data-cv-test='Header.Modal']")
    public WebElement signInModal;

    @FindBy(id = "usernameField")
    public WebElement emailInputBox;

    @FindBy(id = "passwordField")
    public WebElement passwordInputBox;

    @FindBy(css = "button[data-cv='sign-in-submit']")
    public WebElement signInSubmitButton;

    @FindBy(css = "div[data-qa='error-message-container']")
    public WebElement errorMessage;

    @FindBy(css = "div[class^='HeaderMenustyles']>a")
    public WebElement searchCarsButton;
}
